#include "DisplayManager.h"
#include "Shader.h"

#include <iostream>

DisplayManager::DisplayManager(unsigned int inputWidth,
                               unsigned int inputHeight,
                               const std::string &inputWindowName) {
  using namespace std;
  window = SDL_CreateWindow(inputWindowName.c_str(), SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, inputWidth, inputHeight,
                            SDL_WINDOW_OPENGL);
  if (window == NULL) {
    cout << "Error of creating window: " << SDL_GetError() << endl;
  }

  glContext = SDL_GL_CreateContext(window);
  if (glContext == NULL) {
    cout << "Error of creating glContext: " << SDL_GetError() << endl;
  }

  glStatus = glewInit();
  if (glStatus != GLEW_OK) {
    cout << "Error of glewInit(): " << glGetError() << endl;
  }
}

DisplayManager::~DisplayManager() {
  SDL_GL_DeleteContext(glContext);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

bool DisplayManager::IsClosed() { return isClosed; }

void DisplayManager::Paint() {
  glClearColor(0.5, 0.5, 0.5, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // glDrawArrays(GL_TRIANGLES, 0, 3);

  SDL_GL_SwapWindow(window);
}
