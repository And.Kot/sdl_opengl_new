#include "Shader.h"

#include <fstream>
#include <iostream>

#include "Vertices.h"

Shader::Shader(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
{
    vertexShaderSource = strdup(LoadShader(vertexShaderPath).c_str());
    fragmentShaderSource = strdup(LoadShader(fragmentShaderPath).c_str());

    CreateShader(GL_VERTEX_SHADER, vertexShader, vertexShaderSource);
    CreateShader(GL_FRAGMENT_SHADER, fragmentShader, fragmentShaderSource);

    CreateProgram();
    AddArrayBuffer();
}

Shader::~Shader()
{
    if (vertexShader != 0) {
        glDeleteShader(vertexShader);
    }

    if (fragmentShader != 0) {
        glDeleteShader(fragmentShader);
    }
}

void Shader::CreateProgram()
{
    program = glCreateProgram();
    if (program == 0) {
        std::cout << "Error creating a program: " << glGetError() << std::endl;
    }

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    GLuint shaders[2];
    GLsizei count;

    glGetAttachedShaders(program, 2, &count, shaders);

    std::cout << "count = " << count << std::endl;

    glLinkProgram(program);

    GLint linkStatus;
    glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
    if (linkStatus == GL_FALSE) {
        std::cout << "Error linking a program: " << glGetError() << std::endl;

        GLint infoLogSize;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogSize);
        if (infoLogSize > 0) {
            GLsizei size;
            char* errorInfo = new char[infoLogSize];
            glGetProgramInfoLog(program, infoLogSize, &size, errorInfo);
            std::cout << errorInfo << std::endl;
            delete[] errorInfo;
        }
    }

    glDetachShader(program, vertexShader);
    glDetachShader(program, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

std::string Shader::LoadShader(const std::string& path)
{
    std::string line, data;

    std::ifstream inputFile;

    inputFile.open(path, std::ios_base::in | std::ios_base::binary);

    if (inputFile.is_open()) {
        while (std::getline(inputFile, line)) {
            data += line + "\n";
        }
    } else {
        std::cout << "Error reading file" << std::endl;
    }
    return data;
}

void Shader::CreateShader(GLenum shaderType, GLuint& shader,
    const char* shaderSource)
{
    std::string nameShader;
    shaderType == GL_VERTEX_SHADER ? nameShader = "GL_VERTEX_SHADER"
                                   : nameShader = "GL_FRAGMENT_SHADER";

    shader = glCreateShader(shaderType);
    if (shader == 0) {
        std::cout << "Error creating a "
                  << "\"" << nameShader << "\": " << glGetError() << std::endl;
    }

    glShaderSource(shader, 1, &shaderSource, NULL);
    glCompileShader(shader);

    GLint compileStatus;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    if (compileStatus == GL_FALSE) {
        std::cout << "Error creating a "
                  << "\"" << nameShader << "\": " << glGetError() << std::endl;

        GLint sizeLogInfoString;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &sizeLogInfoString);
        if (sizeLogInfoString > 0) {
            char* logInfo = new char[sizeLogInfoString];
            GLint size;

            glGetShaderInfoLog(shader, sizeLogInfoString, &size, logInfo);

            std::cout << logInfo << std::endl;

            delete[] logInfo;
        }
    }
}

void Shader::AddArrayBuffer()
{
    float vertices[6] = {
        -0.9, -0.9,
        0.9, 0.9,
        0.0, 0.9
    };

    buffers.push_back(0);
    glGenBuffers(1, &buffers.at(0));
    glBindBuffer(GL_ARRAY_BUFFER, buffers.at(0));
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), vertices, GL_STREAM_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);
}

void Shader::Use() { glUseProgram(program); }
void Shader::UnUse() { }
