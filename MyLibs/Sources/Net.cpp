#include "Net.h"

#include <iostream>

Net::Net(int nX, int nY)
    : shaderManager("Sources/Shader.vs", "Sources/Shader.fs") {
  Init(nX, nY);
}

Net::~Net() {}

void Net::Init(int nX, int nY) {
  bool flag = true;
  for (int i = 0; i < nY - 1; i++) {
    for (int j = 0; j < nX - 1; j++) {
      if (flag) {
        indices.push_back(i * nX + j);
        indices.push_back(i * nX + j + 1);

        indices.push_back(i * nX + j + 1);
        indices.push_back(i * nX + j + nX + 1);

        indices.push_back(i * nX + j + nX + 1);
        indices.push_back(i * nX + j);

      } else {
        indices.push_back(i * nX + j);
        indices.push_back(i * nX + j + nX + 1);

        indices.push_back(i * nX + j + nX + 1);
        indices.push_back(i * nX + j + nX);

        indices.push_back(i * nX + j + nX);
        indices.push_back(i * nX + j);
      }
    }
    if (flag) {
      flag = false;
      i--;
    } else {
      flag = true;
    }
  }

  float dx = 1.0 / (nX - 1);
  float dy = 1.0 / (nY - 1);
  for (int i = 0; i < nY; i++) {
    for (int j = 0; j < nX; j++) {
      Vertex2D vertex;
      vertex.SetPosition(-0.9 + 1.8 * j * dx, 0.9 - 1.8 * i * dy);
      vertex.SetColor(0, 255, 255, 255);
      vertices.push_back(vertex);
    }
  }

  defaultVertices = vertices;

  // shaderManager.SetShader("Sources/Shader.vs", "Sources/Shader.fs");
  // shaderManager.Use();

  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex2D) * vertices.size(),
               &vertices.front(), GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D),
                        (void*)offsetof(Vertex2D, position));
  glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex2D),
                        (void*)offsetof(Vertex2D, color));

  glGenBuffers(1, &IBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * (indices.size()),
               &indices.front(), GL_STATIC_DRAW);
}

void Net::Update(float mouseX, float mouseY, float mouseRadius) {
  for (int i = 0; i < vertices.size(); i++) {
    float x = vertices.at(i).position.x;
    float y = vertices.at(i).position.y;

    float dx = x - mouseX;
    float dy = y - mouseY;
    if (dx * dx + dy * dy < mouseRadius * mouseRadius) {
      x = x + 0.0025 * dx / (dx * dx + 0.003);
      y = y + 0.0025 * dy / (dy * dy + 0.003);

      vertices.at(i).color = {255, 0, 255, 255};
    }

    x < -1.0 ? x = -1.0 : x;
    x > 1.0 ? x = 1.0 : x;
    y < -1.0 ? y = -1.0 : y;
    y > 1.0 ? y = 1.0 : y;

    vertices.at(i).position.x = x;
    vertices.at(i).position.y = y;
  }

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex2D) * vertices.size(),
                  &vertices.front());

  vertices = defaultVertices;
}

void Net::Draw() {
  shaderManager.Use();
  glBindVertexArray(VAO);
  glDrawElements(GL_LINES, indices.size(), GL_UNSIGNED_INT, nullptr);
  shaderManager.UnUse();
}
