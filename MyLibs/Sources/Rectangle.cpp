#include "Rectangle.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include <Windows.h>
#include <iostream>
#include <string>

Rectangle::Rectangle(float x, float y, float w, float h)
    : shaderManager("Sources/Shader.vs", "Sources/Shader.fs") {
  Init(x, y, w, h);
}

Rectangle::~Rectangle() {}

void Rectangle::Init(float x, float y, float w, float h) {
  vertices[0].SetPosition(x, y);
  vertices[0].SetColor(0, 255, 0, 255);

  vertices[1].SetPosition(x + w, y);
  vertices[1].SetColor(255, 0, 0, 255);

  vertices[2].SetPosition(x + w, y + h);
  vertices[2].SetColor(0, 0, 255, 255);

  vertices[3].SetPosition(x, y + h);
  vertices[3].SetColor(0, 0, 0, 255);

  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D),
                        (void*)offsetof(Vertex2D, position));
  glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex2D),
                        (void*)offsetof(Vertex2D, color));

  glGenBuffers(1, &IBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
               GL_STATIC_DRAW);
}

void Rectangle::Update(float* inPositions, GLubyte* inColor) {
  vertices[0].SetPosition(inPositions[0], inPositions[1]);
  vertices[0].SetColor(inColor[0], inColor[1], inColor[2], inColor[3]);

  vertices[1].SetPosition(inPositions[2], inPositions[3]);
  vertices[1].SetColor(inColor[4], inColor[5], inColor[6], inColor[7]);

  vertices[2].SetPosition(inPositions[4], inPositions[5]);
  vertices[2].SetColor(inColor[8], inColor[9], inColor[10], inColor[11]);

  vertices[3].SetPosition(inPositions[6], inPositions[7]);
  vertices[3].SetColor(inColor[12], inColor[13], inColor[14], inColor[15]);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), &vertices);
}

void Rectangle::Rotate(float angle) {
  shaderManager.Use();
  // glBindVertexArray(VAO);
  rotationMatrix = glm::rotate(mat4(1.0f), angle, vec3(0.0f, 0.0f, 1.0f));

  GLuint location =
      glGetUniformLocation(shaderManager.GetProgramId(), "RotationMatrix");
  if (location >= 0) {
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(rotationMatrix));
  }

  GLsizei bufSize = 15;
  GLint size;
  GLenum type;
  char name[15];
  glGetActiveUniform(shaderManager.GetProgramId(), location, bufSize, nullptr,
                     &size, &type, name);
  shaderManager.UnUse();
}

void Rectangle::Draw() {
  shaderManager.Use();
  glBindVertexArray(VAO);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
  shaderManager.UnUse();
}
