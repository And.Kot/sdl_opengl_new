#include "Texture.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/string_cast.hpp>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include <iostream>

#include "stb_image.h"

Texture::Texture(float x, float y, float w, float h)
    : shaderManager("Sources/Shader.vs", "Sources/Shader.fs") {
  std::cout << "Constructor Texture" << std::endl;
  _x = x;
  _y = y;
  _w = w;
  _h = h;
  Init(x, y, w, h);
}

Texture::Texture(const Texture& another)
    : shaderManager(another.shaderManager) {
  std::cout << "Copy Texture" << std::endl;

  _x = another._x;
  _y = another._y;
  _w = another._w;
  _h = another._h;

  Init(another._x, another._y, another._w, another._h);
}

Texture::Texture(const Texture&& another)
    : shaderManager(another.shaderManager) {
  std::cout << "Move Texture" << std::endl;

  this->_x = another._x;
  this->_y = another._y;
  this->_w = another._w;
  this->_h = another._h;

  Init(another._x, another._y, another._w, another._h);
}

Texture::~Texture() {
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &IBO);
  glDeleteVertexArrays(1, &VAO);
  glDeleteTextures(1, &texture0);
  glDeleteTextures(1, &texture1);
  std::cout << "~Texture" << std::endl;
}

Texture Texture::operator=(const Texture& another) {
  // std::cout << "Equal Texture" << std::endl;
  //*this = new Texture(another);
  // Texture result(another);
  return Texture{another};
}

Texture& Texture::operator=(const Texture&& another) {
  // std::cout << "Assign Moove Texture" << std::endl;

  this->_x = another._x;
  this->_y = another._y;
  this->_w = another._w;
  this->_h = another._h;

  Init(another._x, another._y, another._w, another._h);

  return *this;
}

void Texture::Init(float x, float y, float w, float h) {
  vertices[0].SetPosition(x, y);
  vertices[0].SetColor(0, 255, 0, 255);
  vertices[0].SetTexPos(0.0, 0.0);

  vertices[1].SetPosition(x, y + h);
  vertices[1].SetColor(255, 0, 0, 255);
  vertices[1].SetTexPos(0.0, 1.0);

  vertices[2].SetPosition(x + w, y + h);
  vertices[2].SetColor(0, 0, 255, 255);
  vertices[2].SetTexPos(1.0, 1.0);

  vertices[3].SetPosition(x + w, y);
  vertices[3].SetColor(0, 0, 0, 255);
  vertices[3].SetTexPos(1.0, 0.0);

  ounInterraction = 0;

  // shaderManager("Sources/Shader.vs", "Sources/Shader.fs");

  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);

  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DT),
                        (void*)offsetof(Vertex2DT, position));
  glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex2DT),
                        (void*)offsetof(Vertex2DT, color));
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DT),
                        (void*)offsetof(Vertex2DT, texPosition));

  glGenBuffers(1, &IBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
               GL_STATIC_DRAW);

  LoadImage("Sources/Textures/Poehavshyi.png", "Sources/Textures/Pahom.png");

  Scale(1.0, 1.0);
  Move(0, 0);
  Paint(0.0);
  Rotate(0.0);

  state = false;
}

void Texture::LoadImage(const std::string& path0, const std::string& path1) {
  // tex 0
  glGenTextures(1, &texture0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture0);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, nrChannels;
  stbi_set_flip_vertically_on_load(true);
  unsigned char* data =
      stbi_load(path0.c_str(), &width, &height, &nrChannels, 0);
  if (data) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
  } else {
    std::cout << "Failed to load texture" << std::endl;
  }
  stbi_image_free(data);
  glActiveTexture(0);
  glBindTexture(GL_TEXTURE_2D, 0);

  // tex 1
  glGenTextures(1, &texture1);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture1);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  stbi_set_flip_vertically_on_load(true);
  data = stbi_load(path1.c_str(), &width, &height, &nrChannels, 0);
  if (data) {
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                 GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
  } else {
    std::cout << "Failed to load texture" << std::endl;
  }
  stbi_image_free(data);
  glActiveTexture(0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::Rotate(float angle) {
  shaderManager.Use();
  rotationMatrix = glm::rotate(mat4(1.0f), angle, vec3(0.0f, 0.0f, 1.0f));

  cX = (vertices[0].position.x + vertices[1].position.x +
        vertices[2].position.x + vertices[3].position.x) *
       0.25;
  cY = (vertices[0].position.y + vertices[1].position.y +
        vertices[2].position.y + vertices[3].position.y) *
       0.25;

  for (int i = 0; i < 4; ++i) {
    vertices[i].position.x -= cX;
    vertices[i].position.y -= cY;
  }

  GLuint location =
      glGetUniformLocation(shaderManager.GetProgramId(), "RotationMatrix");
  if (location >= 0) {
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(rotationMatrix));
  }
  Update();

  for (int i = 0; i < 4; ++i) {
    r[i].x = vertices[i].position.x;
    r[i].y = vertices[i].position.y;
    r[i] = glm::rotate(r[i], angle);

    vertices[i].position.x = r[i].x + cX;
    vertices[i].position.y = r[i].y + cY;
  }

  rotationMatrix = glm::rotate(mat4(1.0f), 0.0f, vec3(0.0f, 0.0f, 1.0f));
  glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(rotationMatrix));

  Update();
  shaderManager.UnUse();
}

void Texture::Move(float x, float y) {
  shaderManager.Use();

  MoveMatrix = glm::translate(mat4(1.0f), vec3(x, y, 0.0f));

  GLuint location =
      glGetUniformLocation(shaderManager.GetProgramId(), "MoveMatrix");
  if (location >= 0) {
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(MoveMatrix));
  }

  for (int i = 0; i < 4; ++i) {
    vertices[i].position.x += x;
    vertices[i].position.y += y;
  }
  Update();

  MoveMatrix = glm::translate(mat4(1.0f), vec3(0.0f, 0.0f, 0.0f));
  glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(MoveMatrix));

  shaderManager.UnUse();
}

void Texture::Scale(float x, float y) {
  shaderManager.Use();
  ScaleMatrix = glm::scale(mat4(1.0f), vec3(x, y, 0.0f));

  cX = (vertices[0].position.x + vertices[1].position.x +
        vertices[2].position.x + vertices[3].position.x) *
       0.25;
  cY = (vertices[0].position.y + vertices[1].position.y +
        vertices[2].position.y + vertices[3].position.y) *
       0.25;

  for (int i = 0; i < 4; ++i) {
    vertices[i].position.x -= cX;
    vertices[i].position.y -= cY;
  }

  GLuint location =
      glGetUniformLocation(shaderManager.GetProgramId(), "ScaleMatrix");
  if (location >= 0) {
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(ScaleMatrix));
  }
  Update();

  for (int i = 0; i < 4; ++i) {
    vertices[i].position.x *= x;
    vertices[i].position.y *= y;

    vertices[i].position.x += cX;
    vertices[i].position.y += cY;
  }

  Update();

  ScaleMatrix = glm::scale(mat4(1.0f), vec3(1.0f, 1.0f, 0.0f));
  glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(ScaleMatrix));

  shaderManager.UnUse();
}

void Texture::Paint(float t) {
  shaderManager.Use();

  location = glGetUniformLocation(shaderManager.GetProgramId(), "u_Color");
  if (location >= 0) {
    glUniform4f(location, 1.0 * std::cos(t) * std::cos(t),
                1.0 * std::sin(t + 0.4) * std::sin(t + 0.4),
                1.0 * std::sin(t) * std::sin(t), 1.0);
  }

  shaderManager.UnUse();
}

void Texture::Fill(float r, float g, float b, float a) {
  shaderManager.Use();

  location = glGetUniformLocation(shaderManager.GetProgramId(), "u_Color");
  glUniform4f(location, r, g, b, a);

  shaderManager.UnUse();
}

bool Texture::ChosenByMouse(float xMouse, float yMouse) {
  auto ckeck = [](float x0, float x1, float xM) {
    //    std::cout << "xM - x0 = " << xM - x0 << " x1 - x0 = " << x1 - x0
    //              << std::endl;

    return (!(xM - x0 > x1 - x0) && !(xM - x0 < 0.0)) ||
           (!(xM - x0 < x1 - x0) && !(xM - x0 > 0.0));
  };

  bool temp = false;
  int i = 0;

  // std::cout << std::endl;

  // x
  while (!(temp || i >= 3)) {
    //  std::cout << i << "x: " << temp << " " << xMouse <<
    //    vertices[i].position.x
    //              << " " << vertices[i + 1].position.x << std::endl;
    temp = ckeck(vertices[i].position.x, vertices[i + 1].position.x, xMouse);

    ++i;
  }
  // y
  i = 0;
  if (temp) {
    temp = false;
    while (!(temp || i >= 3)) {
      // std::cout << i << "y: " << temp << " " << yMouse << " "
      //                << vertices[i].position.y << " " << vertices[i +
      //                1].position.y
      //                << std::endl;
      temp = ckeck(vertices[i].position.y, vertices[i + 1].position.y, yMouse);

      ++i;
    }
  } else {
    // std::cout << xMouse << " " << yMouse << " " << temp << std::endl;
    state = temp;
    return temp;
  }

  // std::cout << xMouse << " " << yMouse << " " << temp << std::endl;

  temp == true ? temp = NarrowChosenByMouse(xMouse, yMouse) : temp = false;

  state = temp;
  return temp;
}

void Texture::Update() {
  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), &vertices);
}

void Texture::Draw() {
  shaderManager.Use();

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture0);
  glUniform1i(
      glGetUniformLocation(shaderManager.GetProgramId(), "UserTexture0"), 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, texture1);
  glUniform1i(
      glGetUniformLocation(shaderManager.GetProgramId(), "UserTexture1"), 1);

  glBindVertexArray(VAO);
  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
  shaderManager.UnUse();
}

bool Texture::NarrowChosenByMouse(float xMouse, float yMouse) {
  int count = 0;

  auto ckeckX = [](float x0, float x1, float xM) {
    //    std::cout << " xM < x0 = " << (xM < x0) << "\txM < x1 = " << (xM < x1)
    //              << "\t\t\tresult = " << (xM < x0 || xM < x1) << std::endl;
    return xM < x0 || xM < x1;
  };

  auto ckeckY = [](float y0, float y1, float yM) {
    //    std::cout << " yM > y0 && yM < y1 = " << (yM > y0 && yM < y1)
    //              << "\tyM > y1 && yM < y0 = " << (yM > y1 && yM < y0)
    //              << "\tresult = " << ((yM > y0 && yM < y1) || (yM > y1 && yM
    //              < y0))
    //              << std::endl;
    return (yM > y0 && yM < y1) || (yM > y1 && yM < y0);
  };

  auto ckeckXY = [](float x0, float x1, float y0, float y1, float xM,
                    float yM) {
    bool t =
        (((x0 - xM) * (y1 - y0) + (yM - y0) * (x1 - x0)) > 0 &&
         (y1 - y0) >= 0) ||
        (((x0 - xM) * (y1 - y0) + (yM - y0) * (x1 - x0)) < 0 && (y1 - y0) < 0);

    //    std::cout << "\t\t\t\t\t\tXY_res = " << t << std::endl;
    //    std::cout << ((x0 - xM) * (y1 - y0)) << " " << ((yM - y0) * (x1 - x0))
    //              << " " << ((x0 - xM) * (y1 - y0) + (yM - y0) * (x1 - x0))
    //              << std::endl;

    return t;
  };

  for (int i = 0; i < 3; ++i) {
    //    std::cout << " i = " << i << std::endl;
    if ((ckeckX(vertices[i].position.x, vertices[i + 1].position.x, xMouse) &&
         ckeckY(vertices[i].position.y, vertices[i + 1].position.y, yMouse)) &&
        ckeckXY(vertices[i].position.x, vertices[i + 1].position.x,
                vertices[i].position.y, vertices[i + 1].position.y, xMouse,
                yMouse)) {
      ++count;
    }
  }

  //  std::cout << " i = " << 3 << std::endl;
  if ((ckeckX(vertices[3].position.x, vertices[0].position.x, xMouse) &&
       ckeckY(vertices[3].position.y, vertices[0].position.y, yMouse)) &&
      ckeckXY(vertices[3].position.x, vertices[0].position.x,
              vertices[3].position.y, vertices[0].position.y, xMouse, yMouse)) {
    ++count;
  }
  //  std::cout << " count = " << count << std::endl;
  //  std::cout << std::endl;
  return !(count % 2 == 0);
}

void Texture::PrintVSS() {
  std::cout << "Texture: " << this << "; x = " << vertices[0].position.x
            << "; y = " << vertices[0].position.y << std::endl;
}

void Texture::DetectInterraction(float t) {
  if (ounInterraction > 0) {
    Fill(0.0, 0.0, 0.0, 0.0);
  } else {
    Paint(t);
  }
}
