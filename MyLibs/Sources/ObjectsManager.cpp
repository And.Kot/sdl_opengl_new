#include "ObjectsManager.h"

#include <iostream>

#include "Texture.h"

ObjectsManager::ObjectsManager() {}

void ObjectsManager::CreateTexture(float x, float y, float w, float h) {
  texturies.push_back(Texture(x, y, w, h));
}

void ObjectsManager::GlobalCollisionCalc(std::vector<Texture>& objects) {
  for (int i = 0; i < objects.size(); ++i) {
    for (int j = i + 1; j < objects.size(); ++j) {
      if (CollisionCalc(objects.at(i), objects.at(j))) {
        AddInterraction(objects.at(i));
        AddInterraction(objects.at(j));
      }
    }
  }
}

void ObjectsManager::GlobalCollisionCalc(std::list<Texture>& objects) {
  for (auto it1 = objects.begin(); it1 != objects.end(); ++it1) {
    auto it2 = it1;
    ++it2;
    for (; it2 != objects.end(); ++it2) {
      if (CollisionCalc(*it1, *it2)) {
        AddInterraction(*it1);
        AddInterraction(*it2);
      }
    }
  }
}

bool ObjectsManager::CollisionCalc(Texture& obj1, Texture& obj2) {
  int count = 0;

  for (int i = 0; i < 4; ++i) {
    if (obj1.ChosenByMouse(GetVertex(obj2, i).x, GetVertex(obj2, i).y) ||
        obj2.ChosenByMouse(GetVertex(obj1, i).x, GetVertex(obj1, i).y)) {
      ++count;
    }
  }

  return count > 0;
}

Vertex2DT::Position ObjectsManager::GetVertex(Texture& texture, int number) {
  return texture.vertices[number].position;
}

void ObjectsManager::AddInterraction(Texture& texture) {
  ++texture.ounInterraction;
}

void ObjectsManager::ResetInterraction(std::vector<Texture>& textures) {
  for (int i = 0; i < textures.size(); ++i) {
    textures[i].ounInterraction = 0;
  }
}

void ObjectsManager::ResetInterraction(std::list<Texture>& textures) {
  for (auto& it : textures) {
    it.ounInterraction = 0;
  }
}
