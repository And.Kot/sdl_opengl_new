#include "ShaderManager.h"

#include <fstream>
#include <iostream>

ShaderManager::ShaderManager(const std::string& vertexShaderPath,
                             const std::string& fragmentShaderPath) {
  std::cout << "Constructor Shader" << std::endl;
  SetShader(vertexShaderPath, fragmentShaderPath);
}

ShaderManager::ShaderManager(const ShaderManager& another) {
  std::cout << "CopyShader" << std::endl;

  std::string vertexString = another.vertexShaderSource;
  std::string fragmentString = another.fragmentShaderSource;

  this->vertexShaderSource = another.vertexShaderSource;
  this->fragmentShaderSource = another.fragmentShaderSource;

  this->CreateShader(GL_VERTEX_SHADER, this->vertexShader,
                     another.vertexShaderSource);
  this->CreateShader(GL_FRAGMENT_SHADER, this->fragmentShader,
                     another.fragmentShaderSource);

  this->CreateProgram();
}

ShaderManager::~ShaderManager() {
  glDeleteProgram(program);
  std::cout << "~ShaderManager" << std::endl;
}

void ShaderManager::SetShader(const std::string& vertexShaderPath,
                              const std::string& fragmentShaderPath) {
  vertexShaderSource = strdup(LoadShader(vertexShaderPath).c_str());
  fragmentShaderSource = strdup(LoadShader(fragmentShaderPath).c_str());

  CreateShader(GL_VERTEX_SHADER, vertexShader, vertexShaderSource);
  CreateShader(GL_FRAGMENT_SHADER, fragmentShader, fragmentShaderSource);

  CreateProgram();
}

std::string ShaderManager::LoadShader(const std::string& path) {
  std::string line, data;

  std::ifstream inputFile;

  inputFile.open(path, std::ios_base::in | std::ios_base::binary);

  if (inputFile.is_open()) {
    while (std::getline(inputFile, line)) {
      data += line + "\n";
    }
  } else {
    std::cout << "Error reading file" << std::endl;
  }

  return data;
}

void ShaderManager::CreateShader(GLenum shaderType, GLuint& shader,
                                 const char* shaderSource) {
  std::string nameShader;
  shaderType == GL_VERTEX_SHADER ? nameShader = "GL_VERTEX_SHADER"
                                 : nameShader = "GL_FRAGMENT_SHADER";

  shader = glCreateShader(shaderType);
  if (shader == 0) {
    std::cout << "Error creating a "
              << "\"" << nameShader << "\": " << glGetError() << std::endl;
  }

  glShaderSource(shader, 1, &shaderSource, NULL);
  glCompileShader(shader);

  GLint compileStatus;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
  if (compileStatus == GL_FALSE) {
    std::cout << "Error creating a "
              << "\"" << nameShader << "\": " << glGetError() << std::endl;

    GLint sizeLogInfoString;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &sizeLogInfoString);
    if (sizeLogInfoString > 0) {
      char* logInfo = new char[sizeLogInfoString];
      GLint size;

      glGetShaderInfoLog(shader, sizeLogInfoString, &size, logInfo);

      std::cout << logInfo << std::endl;

      delete[] logInfo;
    }
  }
}

void ShaderManager::CreateProgram() {
  program = glCreateProgram();
  if (program == 0) {
    std::cout << "Error creating a program: " << glGetError() << std::endl;
  }

  glAttachShader(program, vertexShader);
  glAttachShader(program, fragmentShader);

  glLinkProgram(program);

  GLint linkStatus;
  glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
  if (linkStatus == GL_FALSE) {
    std::cout << "Error linking a program: " << glGetError() << std::endl;

    GLint infoLogSize;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogSize);
    if (infoLogSize > 0) {
      GLsizei size;
      char* errorInfo = new char[infoLogSize];
      glGetProgramInfoLog(program, infoLogSize, &size, errorInfo);
      std::cout << errorInfo << std::endl;
      delete[] errorInfo;
    }
  }

  //    std::cout << "program: " << program << "; v: " << vertexShader
  //              << "; f: " << fragmentShader << std::endl;

  glDetachShader(program, vertexShader);
  glDetachShader(program, fragmentShader);

  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
}

void ShaderManager::Use() { glUseProgram(program); }

void ShaderManager::UnUse() { glUseProgram(0); }

GLuint ShaderManager::GetProgramId() { return program; }

GLuint ShaderManager::GetVS() { return vertexShader; }
GLuint ShaderManager::GetFS() { return fragmentShader; }

void ShaderManager::Print() {
  std::cout << "program: " << program << "; v: " << vertexShader
            << "; f: " << fragmentShader << std::endl;
}
