#include "MainLoop.h"

#include <../../ExternalLibraries/ImGui/imgui.h>

#include <cmath>
#include <iostream>

// static PFNGLCREATESHADERPROC glCreateShader = nullptr;

const int FPS = 60;
const int frameDelay = 1000 / FPS;

float time = 0;
int flag = -1;
int flag1 = 0;

MainLoop::MainLoop(unsigned int inputWidth, unsigned int inputHeight,
                   const std::string& inputWindowName)
    : window(SDL_CreateWindow(inputWindowName.c_str(), SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, inputWidth, inputHeight,
                              SDL_WINDOW_OPENGL)),
      glContext(SDL_GL_CreateContext(window)),
      glStatus(glewInit()),
      textures{
          {-1.0, -1.0, 0.2, 0.2},
          {-1.0, 0.8, 0.2, 0.2},
      } {
  isWindowClosed = false;

  width = inputWidth;
  height = inputHeight;

  using namespace std;

  if (window == NULL) {
    cout << "Error of creating window: " << SDL_GetError() << endl;
  }

  if (glContext == NULL) {
    cout << "Error of creating glContext: " << SDL_GetError() << endl;
  }

  if (glStatus != GLEW_OK) {
    cout << "Error of glewInit(): " << glGetError() << endl;
  }

  objManager.GlobalCollisionCalc(textures);

  glClearColor(0.5, 0.2, 0.2, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  // ImGui

  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;

  // setup Dear ImGui style
  // ImGui::StyleColorsDark();

  // setup platform/renderer bindings

  // ImGui_ImplSDL2_Init(window, glContext);
  // ImGui_ImplOpenGL3_Init(glsl_version.c_str());
}

MainLoop::~MainLoop() {
  textures.clear();
  SDL_GL_DeleteContext(glContext);
  SDL_DestroyWindow(window);
}

void MainLoop::Run() {
  while (!isWindowClosed) {
    frameStart = SDL_GetTicks();

    Update();
    Rander();

    frameTime = SDL_GetTicks() - frameStart;
    if (frameDelay > frameTime) {
      SDL_Delay(frameDelay - frameTime);
    }
  }
}

void MainLoop::Update() {
  time += 0.04;
  inputManager.Update(event);
  isWindowClosed = inputManager.ExitStatus();
  inputManager.GetMousePostion(x, y);
  x = (2 * x / width) - 1.0;
  y = -((2 * y / height) - 1.0);

  if (inputManager.GetState(SDLK_i)) {
    std::cout << "textures count: " << textures.size() << std::endl;
  }

  if (inputManager.GetState(SDLK_n)) {
    textures.push_back({x, y, 0.2, 0.2});
    std::cout << "textures count: " << textures.size() << std::endl;
  }

  objManager.ResetInterraction(textures);
  objManager.GlobalCollisionCalc(textures);

  int i = 0;
  for (auto& t : textures) {
    if (t.ChosenByMouse(x, y)) {
      if (inputManager.GetState(SDLK_DOWN)) {
        t.Rotate(0.1);
      }

      if (inputManager.GetState(SDLK_UP)) {
        t.Rotate(-0.1);
      }

      if (inputManager.GetState(SDLK_w)) {
        t.Move(0.0, 0.05);
      }

      if (inputManager.GetState(SDLK_s)) {
        t.Move(0.0, -0.05);
      }

      if (inputManager.GetState(SDLK_a)) {
        t.Move(-0.05, 0.00);
      }

      if (inputManager.GetState(SDLK_d)) {
        t.Move(0.05, 0.0);
      }

      if (inputManager.GetState(SDLK_LEFT)) {
        t.Scale(0.9, 0.9);
      }

      if (inputManager.GetState(SDLK_RIGHT)) {
        t.Scale(1.1, 1.1);
      }

      if (inputManager.GetState(SDLK_h)) {
        t.Init(0.0, 0.0, 0.4, 0.4);
      }

      if (inputManager.GetState(SDLK_b)) {
        flag = i;
      }

      if (inputManager.GetState(SDLK_p)) {
        // t.Scale(1.0, 1.0);
        t.Scale(1.0 + 0.01 * (std::cos(time)), 1.0 + 0.01 * (std::cos(time)));
      }
    }

    t.DetectInterraction(time);
    ++i;
  }

  if (flag >= 0) {
    auto it = textures.begin();
    std::advance(it, flag);
    textures.erase(it);

    flag = -1;
  }
}

void MainLoop::Rander() {
  glClearColor(0.5, 0.2, 0.2, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);

  for (auto& t : textures) {
    t.Draw();
  }

  SDL_GL_SwapWindow(window);
}
