#include "InputManager.h"

#include <iostream>

InputManager::InputManager() {
  exit = false;
  for (int i = 0; i < 422; i++) {
    keys[i] = false;
  }
}

InputManager::~InputManager() {
  Dispose();
}

void InputManager::Dispose() {
  keyMap.clear();
}

void InputManager::Update(SDL_Event& event) {
  while (SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_QUIT:
        exit = true;
        break;
      case SDL_MOUSEMOTION:
        SDL_GetMouseState(&mouseX, &mouseY);
        break;
      case SDL_KEYDOWN:
        KeyDown(event.key.keysym.sym);
        break;
      case SDL_KEYUP:
        KeyUp(event.key.keysym.sym);
        break;
      case SDL_MOUSEBUTTONDOWN:
        KeyDown(event.key.keysym.sym);
        break;
    }
  }
}

bool InputManager::ExitStatus() {
  return exit;
}

void InputManager::PrintMousePosition() {
  std::cout << "x = " << mouseX << "; y = " << mouseY << std::endl;
}

void InputManager::GetMousePostion(float& inX, float& inY) {
  inX = mouseX;
  inY = mouseY;
}

void InputManager::KeyDown(unsigned int keyID) {
  keyMap[keyID] = true;
}

void InputManager::KeyPress(unsigned int keyID) {}

void InputManager::KeyUp(unsigned int keyID) {
  keyMap[keyID] = false;
}

bool InputManager::GetState(unsigned int keyID) {
  return keyMap[keyID];
}
