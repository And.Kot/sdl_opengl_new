#include "Triangle.h"

Triangle::Triangle(Vertex2D inV0,
                   Vertex2D inV1,
                   Vertex2D inV2,
                   const std::string& vertexShaderPath,
                   const std::string& fragmentShaderPath)
    : shaderManager(vertexShaderPath, fragmentShaderPath) {
  vertices[0] = inV0;
  vertices[1] = inV1;
  vertices[2] = inV2;
}

Triangle::~Triangle() {}

// void Triangle::SetTriangle(Vertex2D inV0,
//                           Vertex2D inV1,
//                           Vertex2D inV2,
//                           const std::string& vertexShaderPath,
//                           const std::string& fragmentShaderPath) {
//  shaderManager.SetShader(vertexShaderPath, fragmentShaderPath);
//  vertices[0] = inV0;
//  vertices[1] = inV1;
//  vertices[2] = inV2;
//}

void Triangle::Draw() {
  glBindVertexArray(VAO);
  glDrawArrays(GL_TRIANGLES, 0, 3);
}

void Triangle::ChangeTriangle(float inX, float inY) {
  vertices[0].SetPosition(inX, inY);
  vertices[0].SetColor(25, 0, 123, 255);

  vertices[1].SetPosition(inX + 0.2, inY);
  vertices[1].SetColor(55, 123, 23, 255);

  vertices[2].SetPosition(inX - 0.2, inY - 0.2);
  vertices[2].SetColor(25, 236, 123, 255);
}

void Triangle::Update() {
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), &vertices);
}

void Triangle::Init() {
  vertices[0].SetPosition(0, 0);
  vertices[0].SetColor(25, 0, 123, 255);

  vertices[1].SetPosition(0.8, 0);
  vertices[1].SetColor(55, 123, 23, 255);

  vertices[2].SetPosition(0, 0.8);
  vertices[2].SetColor(25, 236, 123, 255);

  shaderManager.SetShader("Sources/Shader.vs", "Sources/Shader.fs");
  shaderManager.Use();

  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2D),
                        (void*)offsetof(Vertex2D, position));

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex2D),
                        (void*)offsetof(Vertex2D, color));

  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
}
