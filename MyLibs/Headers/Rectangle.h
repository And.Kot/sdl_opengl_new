#pragma once

#include <../../ExternalLibraries/Glm/glm/glm.hpp>
#include <../../ExternalLibraries/Glm/glm/gtc/matrix_transform.hpp>

#include "ShaderManager.h"
#include "Vertices.h"

using glm::mat4;
using glm::vec3;

class __declspec(dllexport) Rectangle {
 public:
  Rectangle() = delete;
  Rectangle(float x, float y, float w, float h);
  ~Rectangle();

  void Init(float x, float y, float w, float h);
  void Update(float* inPositions, GLubyte* inColor);
  void Rotate(float angle);
  void Draw();

 private:
  Vertex2D vertices[4];
  GLuint VBO;
  GLuint IBO;
  GLuint VAO;
  const unsigned int indices[6] = {0, 1, 2, 2, 3, 0};
  ShaderManager shaderManager;

  mat4 rotationMatrix;
};
