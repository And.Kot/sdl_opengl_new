#pragma once

#include <glew.h>

#include <../../ExternalLibraries/Glm/glm/glm.hpp>

struct Vertex2D {
 public:
  struct Position {
    float x;
    float y;
  } position;

  struct Color {
    GLubyte r;
    GLubyte g;
    GLubyte b;
    GLubyte a;
  } color;

  void SetPosition(float inputX, float inputY) {
    position.x = inputX;
    position.y = inputY;
  }

  void SetColor(GLubyte inputR, GLubyte inputG, GLubyte inputB,
                GLubyte inputA) {
    color.r = inputR;
    color.g = inputG;
    color.b = inputB;
    color.a = inputA;
  }
};

struct Vertex2DT {
  struct Position {
    float x;
    float y;
  } position;

  struct Color {
    GLubyte r;
    GLubyte g;
    GLubyte b;
    GLubyte a;
  } color;

  struct TexPosition {
    float x;
    float y;
  } texPosition;

  void SetPosition(float inputX, float inputY) {
    position.x = inputX;
    position.y = inputY;
  }

  void SetColor(GLubyte inputR, GLubyte inputG, GLubyte inputB,
                GLubyte inputA) {
    color.r = inputR;
    color.g = inputG;
    color.b = inputB;
    color.a = inputA;
  }

  void SetTexPos(float inputX, float inputY) {
    texPosition.x = inputX;
    texPosition.y = inputY;
  }
};

struct GlmVertex2DT {
  glm::vec2 position = glm::vec2(0.0, 0.0);

  struct Color {
    GLubyte r;
    GLubyte g;
    GLubyte b;
    GLubyte a;
  } color;

  struct TexPosition {
    float x;
    float y;
  } texPosition;

  void SetPosition(float inputX, float inputY) {
    position.x = inputX;
    position.y = inputY;
  }

  void SetColor(GLubyte inputR, GLubyte inputG, GLubyte inputB,
                GLubyte inputA) {
    color.r = inputR;
    color.g = inputG;
    color.b = inputB;
    color.a = inputA;
  }

  void SetTexPos(float inputX, float inputY) {
    texPosition.x = inputX;
    texPosition.y = inputY;
  }
};
