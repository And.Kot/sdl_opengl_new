#pragma once

#include <glew.h>

#include <list>
#include <string>

#include "InputManager.h"
#include "Net.h"
#include "ObjectsManager.h"
#include "Rectangle.h"
#include "Texture.h"
#include "Triangle.h"

class __declspec(dllexport) Point {
  float a;
  float b;

 public:
  Point() = delete;
  Point(float inA, float inB) {
    a = inA;
    b = inB;
  }
};

class __declspec(dllexport) MainLoop {
 public:
  MainLoop() = delete;
  MainLoop(unsigned int, unsigned int, const std::string&);
  ~MainLoop();
  void Run();
  void SetTriangle();

 private:
  void Update();
  void Rander();

  bool isWindowClosed;

  SDL_Event event;
  SDL_Window* window;
  SDL_GLContext glContext;
  GLenum glStatus;

  InputManager inputManager;
  // std::vector<Texture> textures{};
  std::list<Texture> textures;
  ObjectsManager objManager;

  //  Triangle triangle;
  //  Rectangle rectangle;
  //  Rectangle rectangle2;

  //  Texture texture0;
  //  Texture texture1;

  // Texture textures[2];

  // Net net;

  float x, y;

  int frameStart, frameTime;
  unsigned int width, height;
};
