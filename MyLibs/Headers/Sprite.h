#pragma once

#include <glew.h>

#include <../../ExternalLibraries/Glm/glm/vec2.hpp>
#include <memory>

#include "ShaderManager.h"
#include "Texture.h"

// class __declspec(dllexport) Sprite {
// public:
//  Sprite(const std::shared_ptr<Texture> pTexture,
//         const std::shared_ptr<ShaderManager> pShader,
//         const glm::vec2& position = glm::vec2(0.0f),
//         const glm::vec2& size = glm::vec2(1.0f),
//         const float angle = 0.0);
//  ~Sprite();

//  Sprite(const Sprite&) = delete;
//  Sprite& operator=(const Sprite&) = delete;

//  void SetPosition(const glm::vec2& position);
//  void SetSize(const glm::vec2& size);
//  void Rotate(const float angle);

//  void Draw();
//  void Init(float x, float y, float w, float h);

// private:
//  std::shared_ptr<Texture> _pTexture;
//  std::shared_ptr<ShaderManager> _pShader;
//  glm::vec2 _position;
//  glm::vec2 _size;
//  float _angle;

//  float x, y, width, height;
//  GLuint VAO;
//  GLuint VBO;
//  GLuint IBO;
//  const unsigned int indices[6] = {0, 1, 2, 0, 2, 3};

//  ShaderManager shaderManager;
//};
