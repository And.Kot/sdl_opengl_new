#pragma once

#include <SDL.h>

#include <cstdio>
#include <map>
#include <string>
#include <unordered_map>

class __declspec(dllexport) InputManager {
 public:
  InputManager();
  ~InputManager();

  void Update(SDL_Event&);
  bool ExitStatus();
  void PrintMousePosition();
  void GetMousePostion(float& inX, float& inY);

  void Dispose();

  void KeyDown(unsigned int keyID);
  void KeyPress(unsigned int keyID);
  void KeyUp(unsigned int keyID);

  bool GetState(unsigned int keyID);

 private:
  bool exit;

  int mouseX;
  int mouseY;

  std::unordered_map<unsigned int, bool> keyMap;

  bool keys[322];  // 322 is the number of SDLK_DOWN events
};
