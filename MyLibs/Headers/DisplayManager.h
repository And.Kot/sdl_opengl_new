#pragma once

#include <SDL.h>
#include <glew.h>

#include <string>

class __declspec(dllexport) DisplayManager {
public:
  DisplayManager(unsigned int, unsigned int, const std::string &);
  ~DisplayManager();
  bool IsClosed();
  void Paint();

private:
  SDL_Window *window;
  SDL_GLContext glContext;
  GLenum glStatus;

  bool isClosed;
};
