#pragma once

#include <list>
#include <vector>

#include "Vertices.h"

class Texture;

class __declspec(dllexport) ObjectsManager {
 public:
  ObjectsManager();

  void CreateTexture(float x, float y, float w, float h);
  void GlobalCollisionCalc(std::vector<Texture>& objects);
  void GlobalCollisionCalc(std::list<Texture>& objects);

  Vertex2DT::Position GetVertex(Texture& texture, int number);
  void AddInterraction(Texture& texture);
  void ResetInterraction(std::vector<Texture>& textures);
  void ResetInterraction(std::list<Texture>& textures);

 private:
  std::vector<Texture> texturies;
  bool CollisionCalc(Texture& obj1, Texture& obj2);
};
