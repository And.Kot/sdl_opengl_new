#pragma once

#include "ShaderManager.h"

#include <vector>

class __declspec(dllexport) Net {
 public:
  Net() = delete;
  Net(int nX, int nY);
  ~Net();

  void Init(int nX, int nY);
  void Update(float x, float y, float mouseRadius);
  void Draw();

 private:
  std::vector<unsigned int> indices;
  std::vector<Vertex2D> vertices;
  std::vector<Vertex2D> defaultVertices;

  GLuint VBO;
  GLuint IBO;
  GLuint VAO;

  ShaderManager shaderManager;
};
