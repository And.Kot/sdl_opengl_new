#pragma once

#include <glew.h>

#include <string>

#include "Vertices.h"

class __declspec(dllexport) ShaderManager {
 public:
  ShaderManager() = delete;
  ShaderManager(const std::string& vertexShaderPath,
                const std::string& fragmentShaderPath);
  ~ShaderManager();
  ShaderManager(const ShaderManager& another);

  void SetShader(const std::string& vertexShaderPath,
                 const std::string& fragmentShaderPath);
  void Use();
  void UnUse();

  GLuint GetProgramId();
  GLuint GetVS();
  GLuint GetFS();
  void Print();

 private:
  std::string LoadShader(const std::string& path);
  void CreateShader(GLenum shaderType, GLuint& shader,
                    const char* shaderSource);
  void CreateProgram();

  char* vertexShaderSource;
  char* fragmentShaderSource;
  GLuint program;
  GLuint vertexShader;
  GLuint fragmentShader;
};
