#pragma once

#include <glew.h>

#include <../../ExternalLibraries/Glm/glm/glm.hpp>
#include <../../ExternalLibraries/Glm/glm/gtc/matrix_transform.hpp>
#include <list>
#include <string>
#include <vector>

#include "ObjectsManager.h"
#include "ShaderManager.h"

using glm::mat4;
using glm::vec3;

class __declspec(dllexport) Texture {
 public:
  Texture() = delete;
  Texture(float x, float y, float w, float h);
  Texture(const Texture& another);
  Texture(const Texture&& another);
  ~Texture();

  Texture operator=(const Texture& another);
  Texture& operator=(const Texture&& another);

  void Init(float x, float y, float w, float h);
  void Update(float* inPositions, GLubyte* inColor);
  void Rotate(float angle);
  void Move(float x, float y);
  void Scale(float x, float y);

  void Paint(float t);
  void Fill(float r, float g, float b, float a);

  bool ChosenByMouse(float xMouse, float yMouse);

  void Update();
  void Draw();

  void LoadImage(const std::string& path0, const std::string& path1);

  void PrintVSS();
  void DetectInterraction(float t);

  friend Vertex2DT::Position ObjectsManager::GetVertex(Texture& texture,
                                                       int number);
  friend void ObjectsManager::AddInterraction(Texture& texture);
  friend void ObjectsManager::ResetInterraction(std::vector<Texture>& textures);
  friend void ObjectsManager::ResetInterraction(std::list<Texture>& textures);

 private:
  float _x, _y, _w, _h;
  Vertex2DT vertices[4];
  GLuint VBO;
  GLuint IBO;
  GLuint VAO;
  unsigned int texture0, texture1;
  unsigned int indices[6] = {0, 1, 2, 2, 3, 0};
  ShaderManager shaderManager;

  mat4 rotationMatrix;
  mat4 MoveMatrix;
  mat4 ScaleMatrix;

  GLuint location;

  float cX, cY;
  glm::vec2 r[4];

  bool state;
  int ounInterraction;

  bool NarrowChosenByMouse(float xMouse, float yMouse);
};
