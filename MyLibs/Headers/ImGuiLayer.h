#pragma once

#include "../ImGui/imgui.h"

class __declspec(dllexport) ImGuiLayer {
 public:
  ImGuiLayer();
  ~ImGuiLayer();

  void OnUpdate();
  // void OnEvent(Event& event);

 private:
};
