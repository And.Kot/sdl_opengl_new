#pragma once

#include "ShaderManager.h"
#include "Vertices.h"

class __declspec(dllexport) Triangle {
 public:
  Triangle() = delete;
  Triangle(Vertex2D inV0,
           Vertex2D inV1,
           Vertex2D inV2,
           const std::string& vertexShaderPath,
           const std::string& fragmentShaderPath);
  ~Triangle();

  void Init();
  //  void SetTriangle(Vertex2D inV0,
  //                   Vertex2D inV1,
  //                   Vertex2D inV2,
  //                   const std::string& vertexShaderPath,
  //                   const std::string& fragmentShaderPath);
  void ChangeTriangle(float inX, float inY);
  void Draw();
  void Update();

 private:
  Vertex2D vertices[3];
  GLuint VBO;
  GLuint VAO;
  ShaderManager shaderManager;
};
