#pragma once

#include <glew.h>

#include <string>
#include <vector>

class __declspec(dllexport) Shader {
public:
    Shader(const std::string& vertexShaderPath,
        const std::string& fragmentShaderPath);
    ~Shader();

    void Use();
    void UnUse();

private:
    void CreateProgram();
    void CreateShader(GLenum shaderType, GLuint& shader, const char* shaderSource);
    void AddArrayBuffer();
    std::string LoadShader(const std::string& path);
    GLuint program;
    GLuint vertexShader;
    GLuint fragmentShader;

    const char* vertexShaderSource;
    const char* fragmentShaderSource;

    std::vector<unsigned int> buffers;
};
