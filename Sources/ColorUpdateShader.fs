#version 300 es

precision highp float;

in vec4 FragmentColor;
in vec2 FragmentTexCoord;

uniform sampler2D UserTexture;
uniform sampler2D UserTexture2;

uniform vec4 u_Color;

out vec4 OutColor;

void main() 
{
    OutColor = u_Color;
}

