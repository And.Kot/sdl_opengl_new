#version 300 es

precision highp float;

in vec4 FragmentColor;
in vec2 FragmentTexCoord;

uniform sampler2D UserTexture0;
uniform sampler2D UserTexture1;

uniform vec4 u_Color;

out vec4 OutColor;

void main() 
{
    OutColor = (texture(UserTexture0, FragmentTexCoord) + texture(UserTexture1, FragmentTexCoord)) * FragmentColor * u_Color;
}

