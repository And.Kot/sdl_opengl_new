#version 300 es

layout(location = 0) in vec2 VertexPosition;
layout(location = 1) in vec4 VertexColor;
layout(location = 2) in vec2 VertexTexCoord;

out vec4 FragmentColor;
out vec2 FragmentTexCoord;

uniform mat4 RotationMatrix;
uniform mat4 MoveMatrix;
uniform mat4 ScaleMatrix;

void main()
{

    gl_Position = RotationMatrix * MoveMatrix * ScaleMatrix * vec4(VertexPosition, 0.0, 1.0);
    FragmentColor = VertexColor;
    FragmentTexCoord = VertexTexCoord;
}
