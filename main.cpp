#include "MainLoop.h"
#include "Shader.h"
#include "ShaderManager.h"
#include "Sprite.h"

const unsigned int windowWidth = 1064;
const unsigned int windowHeight = 768;

int main(int, char**) {
  MainLoop mainLoop(windowWidth, windowHeight, "My Window");
  mainLoop.Run();
  return 0;
}
